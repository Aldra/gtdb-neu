﻿GTDB Crew
-----------------------

German Trinity DataBase

Version 0.8

Clientversion 3.1.3

Diese Übersetzung ist kompatibel mit:
		- TrinityCore 5159+
		- TDB Changeset 20+ (und unter Vorbehalt mit jeder weiteren Trinity-DB)

-----------------------
Changes from 0.7

- Viele Änderungen bei Quests, Items, NPC, Gameobjects etc. (vor allem WotLK Support!)
- Added: "command.sql"
- Added: "trinity_string.sql"

 ... (genaueren Changelog wie immer über Sourceforge!)




-----------------------
Kommentar: Der neue Milestone hat lange auf sich warten lassen, wir hoffen, ihr genießt ihn umso mehr :P
           Mal wieder dürfen wir viele neue Developer begrüßen und alte verabschieden!
	   Ich möchte allen Entwicklern und der gesamten Community für die Unterstützung danken!


-----------------------
### GMDB-Crew ###
-----------------------

