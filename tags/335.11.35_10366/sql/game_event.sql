################################################################################
#####                                                                      #####
#####          GGGGGGGG    TTTTTTTTTT     DDDDDDD    BBBBBBB               #####
#####         GG               TT         D     DD   B      B              #####
#####        GG    GGG         TT         D      D   BBBBBBB               #####
#####         GG     GG        TT         D     DD   B      B              #####
#####          GGGGGGG         TT         DDDDDDD    BBBBBBB               #####
#####                             CREW                                     #####
################################################################################
# Copyright (C) 2007-2010 GMDB / GTDB <http://sourceforge.net/projects/gt-db>  #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################




#####	game_event	#####

SET  NAMES 'utf8';  

UPDATE game_event SET description='Sonnenwendfest' WHERE entry=1;
UPDATE game_event SET description='Winterhauchfest' WHERE entry=2;
UPDATE game_event SET description='Dunkelmond-Jahrmarkt (Wälder von Terokkar)' WHERE entry=3;
UPDATE game_event SET description='Dunkelmond-Jahrmarkt (Wald von Elwynn)' WHERE entry=4;
UPDATE game_event SET description='Dunkelmond-Jahrmarkt (Mulgore)' WHERE entry=5;
UPDATE game_event SET description='Neujahr' WHERE entry=6;
UPDATE game_event SET description='Mondfest' WHERE entry=7;
UPDATE game_event SET description='Liebe liegt in der Luft ' WHERE entry=8;
UPDATE game_event SET description='Nobelgartenfest' WHERE entry=9;
UPDATE game_event SET description='Kinderwoche' WHERE entry=10;
UPDATE game_event SET description='Erntedankfest' WHERE entry=11;
UPDATE game_event SET description='Schlotternächte' WHERE entry=12;
UPDATE game_event SET description='Invasion der Elementare' WHERE entry=13;
UPDATE game_event SET description='Anglerwettbewerb im Schlingendorntal Aufruf' WHERE entry=14;
UPDATE game_event SET description='Anglerwettbewerb im Schlingendorntal' WHERE entry=15;
UPDATE game_event SET description='Gurubashiarena' WHERE entry=16;
UPDATE game_event SET description='Invasion der Geißel' WHERE entry=17;
UPDATE game_event SET description='Ruf zu den Waffen: Alteractal!' WHERE entry=18;
UPDATE game_event SET description='Ruf zu den Waffen: Kriegshymnenschlucht!' WHERE entry=19;
UPDATE game_event SET description='Ruf zu den Waffen: Arathibecken!' WHERE entry=20;
UPDATE game_event SET description='Ruf zu den Waffen: Auge des Sturms!' WHERE entry=21;
UPDATE game_event SET description='Krieg von Ahn\'Qiraj' WHERE entry=22;
UPDATE game_event SET description='Dunkelmond-Jahrmarkt wird aufgebaut (Wald von Elwynn)' WHERE entry=23;
-- UPDATE game_event SET description='' WHERE entry=24;
-- UPDATE game_event SET description='' WHERE entry=25;
UPDATE game_event SET description='Braufest' WHERE entry=26;
UPDATE game_event SET description='Nacht beginnt' WHERE entry=27;
UPDATE game_event SET description='Nobelgartenfest' WHERE entry=28;
UPDATE game_event SET description='Rand des Wahnsinns, Gri\'lek' WHERE entry=29;
UPDATE game_event SET description='Rand des Wahnsinns, Hazza\'rah' WHERE entry=30;
UPDATE game_event SET description='Rand des Wahnsinns, Renataki' WHERE entry=31;
UPDATE game_event SET description='Rand des Wahnsinns, Wushoolay' WHERE entry=32;
UPDATE game_event SET description='Arenaturnier' WHERE entry=33;
UPDATE game_event SET description='L70ETC Konzert' WHERE entry=34;
-- UPDATE game_event SET description='' WHERE entry=35;
-- UPDATE game_event SET description='' WHERE entry=36;
-- UPDATE game_event SET description='' WHERE entry=37;
-- UPDATE game_event SET description='' WHERE entry=38;
-- UPDATE game_event SET description='' WHERE entry=39;
-- UPDATE game_event SET description='' WHERE entry=40;
-- UPDATE game_event SET description='' WHERE entry=41;
-- UPDATE game_event SET description='' WHERE entry=42;
-- UPDATE game_event SET description='' WHERE entry=43;
-- UPDATE game_event SET description='' WHERE entry=44;
-- UPDATE game_event SET description='' WHERE entry=45;
-- UPDATE game_event SET description='' WHERE entry=46;
-- UPDATE game_event SET description='' WHERE entry=47;
-- UPDATE game_event SET description='' WHERE entry=48;
-- UPDATE game_event SET description='' WHERE entry=49;
-- UPDATE game_event SET description='' WHERE entry=50;
-- UPDATE game_event SET description='' WHERE entry=51;
-- UPDATE game_event SET description='' WHERE entry=100;
-- UPDATE game_event SET description='' WHERE entry=101;
-- UPDATE game_event SET description='' WHERE entry=102;
-- UPDATE game_event SET description='' WHERE entry=103;
-- UPDATE game_event SET description='' WHERE entry=104;
-- UPDATE game_event SET description='' WHERE entry=105;
-- UPDATE game_event SET description='' WHERE entry=106;
-- UPDATE game_event SET description='' WHERE entry=107;
-- UPDATE game_event SET description='' WHERE entry=108;
-- UPDATE game_event SET description='' WHERE entry=109;
-- UPDATE game_event SET description='' WHERE entry=110;
-- UPDATE game_event SET description='' WHERE entry=111;
-- UPDATE game_event SET description='' WHERE entry=112;
-- UPDATE game_event SET description='' WHERE entry=113;
-- UPDATE game_event SET description='' WHERE entry=114;
-- UPDATE game_event SET description='' WHERE entry=115;
-- UPDATE game_event SET description='' WHERE entry=116;
-- UPDATE game_event SET description='' WHERE entry=117;
-- UPDATE game_event SET description='' WHERE entry=118;
-- UPDATE game_event SET description='' WHERE entry=119;
-- UPDATE game_event SET description='' WHERE entry=120;
-- UPDATE game_event SET description='' WHERE entry=121;
-- UPDATE game_event SET description='' WHERE entry=122;
-- UPDATE game_event SET description='' WHERE entry=123;
UPDATE game_event SET description='Ruf zu den Waffen: Strand der Uralten' WHERE entry=124;
UPDATE game_event SET description='Ruf zu den Waffen: Insel der Eroberung!' WHERE entry=125;
-- UPDATE game_event SET description='' WHERE entry=126;
-- UPDATE game_event SET description='' WHERE entry=127;
-- UPDATE game_event SET description='' WHERE entry=128;
-- UPDATE game_event SET description='' WHERE entry=129;
-- UPDATE game_event SET description='' WHERE entry=130;
-- UPDATE game_event SET description='' WHERE entry=131;

SET NAMES 'latin1';